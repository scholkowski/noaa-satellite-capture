#!/bin/sh

#############################################################################################################
#Parameter 1 -> Satellit Name
#Parameter 2 -> Satellit Frequenz
#Parameter 3 -> Dauer der Aufnahme

#Absoluter Pfad in dem Bilder gespeichert werden
OUTPUT_PATH='/home/pi/Bilder/'

#Absoluter Pfad in dem die Audioaufnahme temporär gespeichert wird
RECORDING_PATH='/home/pi/Audio/'

#############################################################################################################

#Leerzeichen aus Satelliten Name mit '_' ersetzen
SAT_NAME=$(echo -n "$1" | sed -e 's/ /_/g')

FILENAME="`date +%Y-%m-%d_%H-%M`-$SAT_NAME"

#Nehme Audiodatei auf
timeout "$3" rtl_fm -f "$2"M -s 60k -E wav -E deemp -F 9 - | sox -t raw -e signed -c 1 -b 16 -r 60000 - "$RECORDING_PATH$FILENAME".wav rate 11025

#Erstelle Bild
noaa-apt "$RECORDING_PATH$FILENAME".wav -o "$OUTPUT_PATH$FILENAME".png

#Lösche Audio Datei
rm "$RECORDING_PATH$FILENAME".wav