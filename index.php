<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>NOAA Satellites Pictures</title>
		<link rel="stylesheet" href="./css/lightbox.css">
	</head>
	<body style="background-color: Black;">
		<section>
		<div>

		<?php
			function createThumbnail($image, $thumbnail_size, $thumbnail_folder){

				/**
				* Create thumbnail of given image and save it to a specific folder
				*
				* @param string $image Path to source image
				* @param int $thumbnail_size Max size of thumbnail (both width and height)
				* @param string $thumbnail_folder Path to folder where thumbnails are saved
				*/

				//Get image name from Path
				$image_name = basename($image);

				//If thumbnail already exists skip scaling
				if(!file_exists($thumbnail_folder.$image_name)){

					//Get width and height from source image
					list($width, $height) = getimagesize($image);
					//Calculate image ratio
					$ratio = $width/$height;

					//Scale width and height of thumbnail to fit largest side inside boundaries given by thumbnail_size
					if($ration < 1){
						$width_resized = $thumbnail_size;
						$height_resized = $thumbnail_size/$ratio;
					}
					else {
        					$width_resized = $thumbnail_size/$ratio;
        					$height_resized = $thumbnail_size;
					}

					//Create empty thumbnail
					$thumbnail = imagecreatetruecolor($width_resized, $height_resized);
					//Create source image
					$original_image = imagecreatefrompng($image);

					//Copy source image to empty thumbnail
					imagecopyresampled($thumbnail, $original_image, 0, 0, 0, 0, $width_resized, $height_resized, $width, $height);

					//Save thumbnail
					imagepng($thumbnail, $thumbnail_folder.$image_name);
					//Free memory
					imagedestroy($thumbnail);
					imagedestroy($original_image);
				}
			}

			foreach (glob("/home/pi/Bilder/*.png") as $filename) {
				createThumbnail($filename, 150, "./thumbnails/");
				printf("<a class='' href='Bilder/%s' data-lightbox='noaa' data-title='%s'><img class='' src='thumbnails/%s' alt=''/></a>\n\t\t", basename($filename), basename($filename), basename($filename));
				//printf("<li><a href='Bilder/%s'>%s</a></li>", basename($filename), basename($filename));
			}
		?>

		</div>
		</section>
		<script src="./js/lightbox-plus-jquery.js"></script>
	</body>
</html>
