#!/bin/bash

#############################################################################################################
#Parameter 1 -> Name des Satelliten ('NOAA 15', 'NOAA 18','NOAA 19')
#Parameter 2 -> Frequenz des Satelliten in Mhz

#Zeitraum in dem Überflüge berechnet werden sollen ab Ausführung des Skriptes (Angabe in Sekunden)
CALC_DURATION=86400

#Minimale Höhe des Satelitten in Grad
MIN_ELEVATION=20

#Absoluter Pfad zum record_satellite.sh Skript
RECORDING_SCRIPT_PATH='/home/pi/noaa-satellite-capture/record_satellite.sh'

#Absoluter Pfad zur weather.tle
WEATHER_TLE_PATH='/home/pi/noaa-satellite-capture/weather.tle'

#############################################################################################################

#Startzeit für Berechnungen
CALC_START_TIME=`date '+%s'`

#Endzeit für Berechnungen
let CALC_END_TIME=$CALC_START_TIME+$CALC_DURATION

#Startzeit des aktuellen Fensters das geprüft wird
CURRENT_WINDOW=$CALC_START_TIME

#Setzte die Endzeit des "letzten" Überfluges temporär 3600 in die Vergangenheit
#um die Abfrage in der Schleife für den ersten Satelliten zu erfüllen
let OLD_END_TIME=$CALC_START_TIME-3600

#Gebe Satelliten Name aus
printf "%s:\n" "$1"

while [ $CURRENT_WINDOW -le $CALC_END_TIME ]; do

	#Beginn des Überfluges als Unix Zeitstempel
	START_TIME=`predict -t "$WEATHER_TLE_PATH" -p "$1" "$CURRENT_WINDOW" | head -1 | awk '{print $1}'`
	#Ende des Überfluges als Unix Zeitstempel
	END_TIME=`predict -t "$WEATHER_TLE_PATH" -p "$1" "$CURRENT_WINDOW" | tail -1 | awk '{print $1}'`
	#Höhe des Überfluges in Grad
	ELEVATION=`predict -t "$WEATHER_TLE_PATH" -p "$1" "$CURRENT_WINDOW" | awk -v max=0 '{if($5>max){max=$5}}END{print max}'`
	#Dauer des Überfluges in Sekunden
	let DURATION=$END_TIME-$START_TIME
	
	if [ $START_TIME -gt $OLD_END_TIME ] && [ $ELEVATION -ge $MIN_ELEVATION ];
	then
		#Dieser Überflug war gültig
		#Lege AT Job an
		printf "'%s' '%s' '%s' '%s'\n" "$RECORDING_SCRIPT_PATH" "$1" "$2" "$DURATION" | at `date -d @"$START_TIME" '+%H:%M'`
		
		#Gebe Überflug aus
		printf "Start         : %s\nEnd           : %s\nMax Elevation : %s°\nDuration      : %ss\n" "`date -d @"$START_TIME" '+%D %H:%M'`" "`date -d @"$END_TIME" '+%D %H:%M'`" "$ELEVATION" "$DURATION"
		
		#Schiebe Fenster auf die Endzeit dieses Überfluges + 1800s als sicherheitsfenster
		let CURRENT_WINDOW=$END_TIME+1800
	else
		#Dieser Überflug ist ungültig
		#Schiebe Fenster 1800s weiter
        let CURRENT_WINDOW=$CURRENT_WINDOW+1800
	fi
	OLD_END_TIME=$END_TIME
done
