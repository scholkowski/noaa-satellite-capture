# NOAA Satellite Capture

Automatically record NOAA weather Satellites 15,18 and 19.

Based on [this](https://esero.de/wp-interface/wp-content/uploads/Raspberry_Pi_NOAA_Satellitenbilder_Sch%C3%BClermaterial.pdf) work.

# Dependencies
- [predict](https://github.com/kd2bd/predict)
- [noaa-apt](https://github.com/martinber/noaa-apt)
- [rtl-sdr-blog tools and drivers](https://github.com/rtlsdrblog/rtl-sdr-blog)
- [at](https://en.wikipedia.org/wiki/At_(command))
