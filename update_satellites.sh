#!/bin/bash

#############################################################################################################

#Absoluter Pfad zum schedule_satellite.sh Skript
SCHEDULE_SCRIPT_PATH='/home/pi/noaa-satellite-capture/schedule_satellite.sh'

#Absoluter Pfad zur weather.tle
WEATHER_TLE_PATH='/home/pi/noaa-satellite-capture/weather.tle'

#Absoluter Pfad zur weather.txt
WEATHER_TXT_PATH='/home/pi/noaa-satellite-capture/weather.txt'

#############################################################################################################

# Update Satellite Information
wget -qr https://www.celestrak.com/NORAD/elements/weather.txt -O "$WEATHER_TXT_PATH"
grep 'NOAA 15' "$WEATHER_TXT_PATH" -A 2 > "$WEATHER_TLE_PATH"
grep 'NOAA 18' "$WEATHER_TXT_PATH" -A 2 >> "$WEATHER_TLE_PATH"
grep 'NOAA 19' "$WEATHER_TXT_PATH" -A 2 >> "$WEATHER_TLE_PATH"

#Delete all AT Jobs
for i in `atq | awk '{print $1}'`;do atrm $i;done

#Schedule Satellites
"$SCHEDULE_SCRIPT_PATH" 'NOAA 15' '137.62'
"$SCHEDULE_SCRIPT_PATH" 'NOAA 18' '137.9125'
"$SCHEDULE_SCRIPT_PATH" 'NOAA 19' '137.1'
